---
# gitlab-yaml-shellcheck: main=../../cki_pipeline.yml

.test-set-env-var: |
  # Export var for UPT to dump kcidb data, it's the only value we're missing when we don't use token.
  job_created_at="$(date_utc)"
  export job_created_at

.test-write-restraint-config: |
  # ⚙ Set up the restraint configuration.
  # shellcheck disable=SC2046 # FIXME warning disabled to enable linting: warning: Quote this to prevent word splitting. [SC2046]
  eval $(ssh-agent)
  if [ -v CKI_TEAM_AUTOMATION_PRIVKEY ]; then
    echo "${CKI_TEAM_AUTOMATION_PRIVKEY}" | tr -d '\r' | ssh-add -
  fi
  if [ -v AWS_UPT_SSH_PRIVATE_KEY ]; then
    echo "${AWS_UPT_SSH_PRIVATE_KEY}" | tr -d '\r' | ssh-add -
  fi

.test-write-beaker-config: |
  # ⚙ Set up the Beaker configuration.
  if [[ -v BEAKER_URL ]]; then
    cki_echo_heading "Preparing Beaker instance authentication"
    mkdir -p "${HOME}/.beaker_client"
    echo "${BEAKER_CA_CERT}" > "${HOME}/.beaker_client/cert"
    append_file "${HOME}/.beaker_client/config" "
      HUB_URL = \"${BEAKER_URL}\"
      CA_CERT = \"${HOME}/.beaker_client/cert\"
      DEBUG_XMLRPC = False
    "

    # include Beaker CA certificate in CA bundle for Python requests and curl
    BEAKER_REQUESTS_CA_BUNDLE=${HOME}/.beaker_client/ca-bundle.crt
    cp "${REQUESTS_CA_BUNDLE}" "${BEAKER_REQUESTS_CA_BUNDLE}"
    echo "${BEAKER_CA_CERT}" >> "${BEAKER_REQUESTS_CA_BUNDLE}"
    export REQUESTS_CA_BUNDLE=${BEAKER_REQUESTS_CA_BUNDLE}
    export CURL_CA_BUNDLE=${BEAKER_REQUESTS_CA_BUNDLE}

    # Kerberos authentication
    if [[ -n ${BEAKER_REALM:-} ]] && [[ -n ${BEAKER_KEYTAB:-} ]] && [[ -n ${BEAKER_PRINCIPAL:-} ]]; then
      cki_echo_notify "  using Kerberos credentials for ${BEAKER_PRINCIPAL}@${BEAKER_REALM}"
      base64 --decode <<< "${BEAKER_KEYTAB}" > "${HOME}/keytab"
      append_file "${HOME}/.beaker_client/config" "
        AUTH_METHOD = \"krbv\"
        KRB_REALM = \"${BEAKER_REALM}\"
        KRB_KEYTAB = \"${HOME}/keytab\"
        KRB_PRINCIPAL = \"${BEAKER_PRINCIPAL}\"
      "
    # user/password authentication
    elif [[ -n ${BEAKER_USERNAME:-} ]] && [[ -n ${BEAKER_PASSWORD:-} ]]; then
      cki_echo_notify "  using user/password credentials for ${BEAKER_USERNAME}"
      append_file "${HOME}/.beaker_client/config" "
        AUTH_METHOD = \"password\"
        USERNAME = \"${BEAKER_USERNAME}\"
        PASSWORD = \"${BEAKER_PASSWORD}\"
      "
    else
      cki_echo_error "  no valid credentials found"
      exit 1
    fi

    # 💀 Download the automatically generated Beaker list of excluded hostnames.
    curl --config "${CKI_CURL_CONFIG_FILE}" --output "${CI_PROJECT_DIR}/${EXCLUDED_HOSTNAMES_PATH}" "${beaker_excluded_hostnames_url}"
  fi

.test-notification: |
  # 💌 Send a notification for certain builds before submitting to Beaker.

  PRETEST_NOTIFY=1

  if ! is_true "${send_pre_test_notification}"; then
    echo "Pre-test notifications not enabled for this pipeline"
    PRETEST_NOTIFY=0
  fi

  # Skip notifications if the owner is not set.
  if [ -z "${submitter}" ]; then
    echo "Skipping pre-test notification: Job owner not set"
    PRETEST_NOTIFY=0
  fi

  # Don't send emails if this is a retriggered pipeline.
  if ! is_production; then
    echo "Skipping pre-test notification: Re-triggered pipeline"
    PRETEST_NOTIFY=0
  fi

  # Only send notifications for non-debug x86_64 builds - we don't want to spam people multiple times
  # TODO: https://gitlab.com/cki-project/pipeline-definition/-/issues/133
  if is_true "${DEBUG_KERNEL}" || [[ $(kcidb_build get architecture) != "x86_64" ]]; then
    echo "Skipping pre-test notification: not a non-debug x86_64 build"
    PRETEST_NOTIFY=0
  fi

  # Send pre-test notification if all conditions are met.
  if [[ ${PRETEST_NOTIFY} = 1 ]]; then
    export KERNEL_RELEASE
    KERNEL_RELEASE=$(kcidb_checkout get misc/kernel_version)
    # Craft a friendly little email to kernel developers to let them know that
    # their kernel is off to the mysterious world of Beaker for testing.
    append_file "pre-test-notification.txt" "
      Hello,

      The CKI team is submitting your kernel build (${KERNEL_RELEASE}) for further
      testing in Beaker. All the testing jobs are listed here:

      ${BEAKER_URL}/jobs/?jobsearch-0.table=Whiteboard&jobsearch-0.operation=contains&jobsearch-0.value=cki%40gitlab%3A${CI_PIPELINE_ID}

      Sincerely yours,
      CKI Project Team 🤖
      cki-project@redhat.com
    "

    echo "Sending pre-test notification email to ${submitter}"
    if ! loop mailx -v -r "cki-project@redhat.com" \
              -c "${DEFAULT_EMAIL}" \
              -s "[${KERNEL_RELEASE}] CKI testing is starting" \
              -S smtp="smtp.corp.redhat.com" \
              "${submitter}" < pre-test-notification.txt ; then
      cki_echo_error "Cannot send notification to ${submitter}!"
    fi
  fi

.test-start-kernel-testing: |
  # 🏃 Start the kernel testing.
  # GitLab will get upset if there is no output on the console for a while.

  # Check if the kernel is supported first and return if not
  if kcidb_build get misc/testing_skipped_reason | grep -q unsupported ; then
    cki_echo_error "This kernel is not supported, aborting"
    exit 0
  fi

  # Replace the job ID placeholder in the Beaker XML with the actual test job ID
  sed -i "s/GITLAB_JOB_ID_PLACEHOLDER/${CI_JOB_ID}/" "${BEAKER_TEMPLATE_PATH}"

  # Extract automotive configuration
  if [[ -f "${AUTOMOTIVE_CONFIGURATION_PATH}" ]]; then
    cki_echo_heading "Using automotive configuration from ${AUTOMOTIVE_CONFIGURATION_PATH}"
    export AWS_UPT_IMAGE_ID
    AWS_UPT_IMAGE_ID=$(jq -r --arg arch "${ARCH_CONFIG}" --arg img_name "cki" --arg img_type "ostree" \
                      'first(.aws[] | select(.arch == $arch and .image_name == $img_name and .image_type == $img_type)) | .image_id' \
                      < "${AUTOMOTIVE_CONFIGURATION_PATH}")
    echo "  image ID: ${AWS_UPT_IMAGE_ID}"
  fi

  KEEP_JOB_ALIVE="gitlab-dont-kill-me-I-am-waiting-for-resources"
  while [ -n "${KEEP_JOB_ALIVE}" ]; do
    sleep 10s
    echo -ne "\0"
    KEEP_JOB_ALIVE=$(pgrep "upt|rcclient" || true);
  done
  provision_and_test_by_upt \
      "${VENV_PY3}" \
      "${EXCLUDED_HOSTNAMES_PATH}" \
      "${BEAKER_TEMPLATE_PATH}" \
      "${UPT_YAML}" \
      "${UPT_OUTPUT_DIR}" \
      "${test_runner}" \
      "${test_priority}"

.test-generate-coverage-result: |
  if is_true "${coverage}" && ! is_true "${skip_beaker}"; then
    cki_echo_heading "Coverage is enabled, we're going to merge kcov files and generate html result"
    cki_echo_heading "Searching for kcov files"
    kcov_list_files=()
    while IFS=  read -r -d $'\0'; do
        kcov_list_files+=("${REPLY}")
    done < <(find "${UPT_RESULTS}"* -name 'kcov.*.info' -print0)

    if [ ${#kcov_list_files[@]} -eq 0 ] ; then
      cki_echo_error "Kcov files not found, HTML summary will not be generated"
      exit 1
    else
      cki_echo_heading "Merging kcov files"
      lcov_command=(lcov --output-file "${KCOV_RAW_MERGE_PATH}")
      for elt in "${kcov_list_files[@]}"; do
          lcov_command+=(--add-tracefile "${elt}")
      done
      echo "  Running lcov with: ${lcov_command[*]}"
      "${lcov_command[@]}"
      # Exclude /usr/lib/gcc to restrict coverage output to kernel sources
      lcov_purge_info_cmd=(lcov --remove "${KCOV_RAW_MERGE_PATH}" --output-file "${KCOV_SANITIZED_MERGE_PATH}" '/usr/lib/gcc/*')
      echo "  Running lcov to remove non kernel directories with: ${lcov_purge_info_cmd[*]}"
      "${lcov_purge_info_cmd[@]}"

      kernel_arch=$(kcidb_build get architecture)
      cki_echo_heading "Getting kernel-gcov files for arch: ${kernel_arch}"
      dnf config-manager --add-repo "$(get_output_file_link build kernel_repofile_url)"
      mkdir /tmp/kernel-gcov
      dnf install -y --downloadonly --downloaddir=/tmp/kernel-gcov \
        --forcearch "${kernel_arch}" \
        --disablerepo=* --enablerepo=cki* \
        kernel*-gcov 2>&1 | ts -s >> "${CI_PROJECT_DIR}/${COVERAGELOG_PATH}"
      # cpio doesn't have an option to select the output directory
      # The extracted files are in /tmp/rpmbuild/BUILD
      cd /
      rpm2cpio /tmp/kernel-gcov/kernel*-gcov*.rpm | cpio -idm
      rm -fr /tmp/kernel-gcov/
      cd "${CI_PROJECT_DIR}"

      cki_echo_heading "Generating html converage result"
      genhtml_cmd=(genhtml --title "Coverage of kernel tests" --show-details --output-directory "${COVERAGE_REPORT_PATH}" --keep-descriptions --legend --highlight "${KCOV_SANITIZED_MERGE_PATH}")
      cki_echo_notify "Running genhtml with: ${genhtml_cmd[*]}"
      if ! "${genhtml_cmd[@]}" 2>&1 | ts -s | tee -a "${CI_PROJECT_DIR}/${COVERAGELOG_PATH}" | tail -n 10; then
        cki_echo_error "couldn't generate HTML summary for kernel coverage."
        exit 1
      fi
      # Create a tarball file with the test logs, coverage report and an info file with the pipeline and job url,
      # architecture and KDIR (directories selected for coverage).
      cki_echo_heading "Generating coverage tarball"
      temp_directory=$(mktemp -d)
      cp -a "${UPT_RESULTS}"* "${temp_directory}"
      cp -a "${COVERAGE_REPORT_PATH}" "${temp_directory}"
      {
        echo "# WARNING: this file is only for human consumption, file structure might change in the future!"
        echo "PIPELINE=${CI_PIPELINE_URL}"
        echo "JOB=${CI_JOB_URL}"
        echo "ARCH=${ARCH_CONFIG}"
        find "${ARTIFACTS_DIR}" -name kcov.conf -exec grep KDIR {} \; -quit
      } > "${temp_directory}/info.txt"
      tar --directory "${temp_directory}" --create --gzip --file "${COVERAGE_TARBALL_PATH}" .
      rm -fr "${temp_directory}"
      cp "${KCOV_SANITIZED_MERGE_PATH}" "${COVERAGE_BASE_PATH}"
      KCOV_SANITIZED_MERGE_FILE=$(basename -- "${KCOV_SANITIZED_MERGE_PATH}")
      coverage_html_link="$(browsable_artifact_url "${COVERAGE_REPORT_PATH}/index.html")"
      coverage_tarball_link="$(artifact_url "${COVERAGE_TARBALL_PATH}")"
      coverage_raw_link="$(artifact_url "${COVERAGE_BASE_PATH}/${KCOV_SANITIZED_MERGE_FILE}")"
      cki_echo_notify "Coverage raw file in ${coverage_raw_link}"
      cki_echo_notify "Coverage HTML summary available in ${coverage_html_link}"
      cki_echo_notify "Coverage tarball available in ${coverage_tarball_link}"
      cki_echo_heading "Updating coverage files to KCIDB file."
      kcidb_build append-dict output_files name=coverage_html url="${coverage_html_link}"
      kcidb_build append-dict output_files name=coverage_tarball url="${coverage_tarball_link}"
      cki_echo_heading "Cleaning up"
      rm -fr /opt/ckibuild
      rm -fr "${KCOV_SANITIZED_MERGE_PATH}" "${KCOV_RAW_MERGE_PATH}"
    fi
  fi

.test:
  extends: [.with_artifacts, .with_retries, .with_python_image]
  stage: test
  variables:
    ARTIFACTS: "artifacts/*.xml artifacts/testid* ${EXCLUDED_HOSTNAMES_PATH} ${TEST_TASKS_PATH} ${UPT_YAML}* ${UPT_RESULTS}* ${UPT_OUTPUT_LOG} ${UPT_LOG} ${COVERAGE_BASE_PATH} ${COVERAGELOG_PATH}"
  before_script:
    - !reference [.common-before-script]
  script:
    - !reference [.test-set-env-var]
    - !reference [.test-write-restraint-config]
    - !reference [.test-write-beaker-config]
    - !reference [.test-notification]
    - !reference [.test-start-kernel-testing]
    - !reference [.test-generate-coverage-result]
    - |
      echo "Collect kcidb Test dumpfiles..."
      "${VENV_PY3}" -m cki.kcidb.adjust_dumpfiles || true
    - kcidb_missing_tests
    - |
      for test_id in $(jq -r '.tests[]?.id' "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"); do
        upload_test_bkr_xml "${test_id}"
      done
    - |
      # If known issue detection is not available, check KCIDB file for results without known issues.
      if is_true "${skip_results}" ; then
        test_summary="$("${VENV_PY3}" -m cki.kcidb.get_test_summary "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}")"

        if [[ "${test_summary}" == 'FAIL' ]] ; then
          cki_echo_error "Test summary is ${test_summary}. Please review the failed tests above."
          exit 1
        elif [[ "${test_summary}" == 'ERROR' ]] ; then
          cki_echo_notify "Test summary is ${test_summary}. Please review the errored tests above."
          # Don't fail on errors.
          # See: https://gitlab.com/cki-project/pipeline-definition/-/issues/193
          exit 0
        else
          cki_echo_heading "Test summary is ${test_summary}."
          exit 0
        fi
      fi
  after_script:
    - !reference [.common-after-script-head]
    - !reference [.common-after-script-tail]
  tags:
    - ${TAG_NON_PRODUCTION_PREFIX}pipeline-test-runner
  rules:
    - !reference [.skip_without_stage]
    - !reference [.skip_without_debug]
    - !reference [.skip_without_architecture]
    - when: on_success

test i686:
  extends: [.test, .i686_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      setup i686
  needs:
    - {artifacts: false, job: prepare python}
    - {artifacts: false, job: setup i686}

test x86_64:
  extends: [.test, .x86_64_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      setup x86_64
  needs:
    - {artifacts: false, job: prepare python}
    - {artifacts: false, job: setup x86_64}

test x86_64 debug:
  extends: [.test, .x86_64_variables, .debug_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      setup x86_64 debug
  needs:
    - {artifacts: false, job: prepare python}
    - {artifacts: false, job: setup x86_64 debug}

test ppc64le:
  extends: [.test, .ppc64le_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      setup ppc64le
  needs:
    - {artifacts: false, job: prepare python}
    - {artifacts: false, job: setup ppc64le}

test ppc64le debug:
  extends: [.test, .ppc64le_variables, .debug_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      setup ppc64le debug
  needs:
    - {artifacts: false, job: prepare python}
    - {artifacts: false, job: setup ppc64le debug}

test aarch64:
  extends: [.test, .aarch64_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      setup aarch64
  needs:
    - {artifacts: false, job: prepare python}
    - {artifacts: false, job: setup aarch64}

test aarch64 debug:
  extends: [.test, .aarch64_variables, .debug_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      setup aarch64 debug
  needs:
    - {artifacts: false, job: prepare python}
    - {artifacts: false, job: setup aarch64 debug}

test ppc64:
  extends: [.test, .ppc64_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      setup ppc64
  needs:
    - {artifacts: false, job: prepare python}
    - {artifacts: false, job: setup ppc64}

test s390x:
  extends: [.test, .s390x_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      setup s390x
  needs:
    - {artifacts: false, job: prepare python}
    - {artifacts: false, job: setup s390x}

test s390x debug:
  extends: [.test, .s390x_variables, .debug_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      setup s390x debug
  needs:
    - {artifacts: false, job: prepare python}
    - {artifacts: false, job: setup s390x debug}

test riscv64:
  extends: [.test, .riscv64_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      setup riscv64
  needs:
    - {artifacts: false, job: prepare python}
    - {artifacts: false, job: setup riscv64}

test riscv64 debug:
  extends: [.test, .riscv64_variables, .debug_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare python
      setup riscv64 debug
  needs:
    - {artifacts: false, job: prepare python}
    - {artifacts: false, job: setup riscv64 debug}
