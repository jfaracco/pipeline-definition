#!/bin/bash
# shellcheck disable=SC2034,SC2154  # variable appears unused/is referenced but not assigned
# shellcheck disable=SC2312         # masking of return values

eval "$(shellspec - -c) exit 1"

Describe 'kpet_generate_args'
    Include tests/source_functions.sh
    Include tests/helpers.sh
    Set 'errexit:on' 'nounset:on' 'pipefail:on'

    KPET_VARIABLES=''
    KPET_ADDITIONAL_VARIABLES=''

    # hack to get to an existing known file name
    CI_PROJECT_DIR=/etc
    SUBSYSTEM_SOURCES_FILE_LIST_PATH=hostname

    Describe 'variable arguments array'

        It 'is empty without any variables'
            When call kpet_generate_args '' ''
            The status should be success
            The array kpet_variable_args should be empty
        End

        It 'ignores non-existent variables'
            KPET_VARIABLES='nonexisting'
            When call kpet_generate_args '' ''
            The status should be success
            The array kpet_variable_args should be empty
        End

        It 'includes existing variables'
            KPET_VARIABLES='variable'
            variable=value
            When call kpet_generate_args '' ''
            The status should be success
            The array kpet_variable_args should be values \
                '--variable' 'variable=value'
        End

        It 'accepts variables with spaces'
            KPET_VARIABLES='variable'
            variable='spacy value'
            When call kpet_generate_args '' ''
            The status should be success
            The array kpet_variable_args should be values \
                '--variable' 'variable=spacy value'
        End

        It 'includes multiple variables'
            KPET_VARIABLES='variable1 variable2'
            variable1='value 1'
            variable2='value 2'
            When call kpet_generate_args '' ''
            The status should be success
            The array kpet_variable_args should be values \
                '--variable' 'variable1=value 1' '--variable' 'variable2=value 2'
        End

        It 'includes additional variables'
            KPET_ADDITIONAL_VARIABLES='variable'
            variable=value
            When call kpet_generate_args '' ''
            The status should be success
            The array kpet_variable_args should be values \
                '--variable' 'variable=value'
        End

        It 'has the kdump server disabled on aws'
            test_runner=aws
            When call kpet_generate_args '' ''
            The status should be success
            The array kpet_variable_args should be values \
                '--variable' 'kdump_server='
        End
    End

    Describe 'noarch arguments array'
        It 'is set correctly'
            When call kpet_generate_args 'comps' ''
            The status should be success
            The array kpet_noarch_args should be values \
                '--tree' 'fedora' '--components' 'comps' '--high-cost' 'triggered'
        End

        It 'contains the tests regex if specified'
            tests_regex='regex'
            When call kpet_generate_args '' ''
            The status should be success
            The array kpet_noarch_args should be values \
                '--tree' 'fedora' '--components' '' '--high-cost' 'triggered' '--tests' 'regex'
        End
    End

    Describe 'singlearch arguments array'
        It 'is set correctly'
            patch_urls=urls
            When call kpet_generate_args '' 'sets'
            The status should be success
            The array kpet_singlearch_args should be values \
                '--arch' 'x86_64' '--mboxes' 'urls' '--file-list' '/etc/hostname' '--sets' 'sets' '--domains' 'available'
        End

        It 'deals with empty patch_urls'
            patch_urls=
            When call kpet_generate_args '' 'sets'
            The status should be success
            The array kpet_singlearch_args should be values \
                '--arch' 'x86_64' '--mboxes' '' '--sets' 'sets' '--domains' 'available'
        End

        It 'deals with multiple domains'
            domains='one two three'
            When call kpet_generate_args '' ''
            The status should be success
            The array kpet_singlearch_args should be values \
                '--arch' 'x86_64' '--mboxes' '' '--sets' '' \
                '--domains' 'one' '--execute' \
                '--domains' 'two' '--execute' \
                '--domains' 'three'
        End

        It 'deals with extra baseline run set patterns'
            patch_urls='urls'
            extra_baseline_run_set_patterns='sets1 sets2'
            When call kpet_generate_args '' 'sets'
            The status should be success
            The array kpet_singlearch_args should be values \
                '--arch' 'x86_64' \
                '--mboxes' '' '--sets' 'sets1' '--domains' 'available' '--execute' \
                '--mboxes' '' '--sets' 'sets2' '--domains' 'available' '--execute' \
                '--mboxes' 'urls' '--file-list' '/etc/hostname' '--sets' 'sets' '--domains' 'available'
        End

        It 'deals with both multiple domains and extra baseline run set patterns'
            patch_urls='urls'
            extra_baseline_run_set_patterns='sets1 sets2'
            domains='one two'
            When call kpet_generate_args '' 'sets'
            The status should be success
            The array kpet_singlearch_args should be values \
                '--arch' 'x86_64' \
                '--mboxes' '' '--sets' 'sets1' '--domains' 'one' '--execute' \
                '--domains' 'two' '--execute' \
                '--mboxes' '' '--sets' 'sets2' '--domains' 'one' '--execute' \
                '--domains' 'two' '--execute' \
                '--mboxes' 'urls' '--file-list' '/etc/hostname' '--sets' 'sets' '--domains' 'one' '--execute' \
                '--domains' 'two'
        End
    End

    Describe 'multiarch arguments array'
        It 'is set correctly'
            When call kpet_generate_args '' '' 's390x' 'arm64'
            The status should be success
            The array kpet_multiarch_args should be values \
                '--arch' 's390x' '--mboxes' '' '--sets' '' '--domains' 'available' '--execute' \
                '--arch' 'arm64' '--mboxes' '' '--sets' '' '--domains' 'available'
        End

        It 'deals with multiple domains'
            domains='one two'
            When call kpet_generate_args '' '' 's390x' 'arm64'
            The status should be success
            The array kpet_multiarch_args should be values \
                '--arch' 's390x' '--mboxes' '' '--sets' '' \
                '--domains' 'one' '--execute' \
                '--domains' 'two' '--execute' \
                '--arch' 'arm64' '--mboxes' '' '--sets' '' \
                '--domains' 'one' '--execute' \
                '--domains' 'two'
        End

        It 'deals with extra baseline run set patterns'
            patch_urls='urls'
            extra_baseline_run_set_patterns='sets1 sets2'
            When call kpet_generate_args '' 'sets' 's390x' 'arm64'
            The status should be success
            The array kpet_multiarch_args should be values \
                '--arch' 's390x' \
                '--mboxes' '' '--sets' 'sets1' '--domains' 'available' '--execute' \
                '--mboxes' '' '--sets' 'sets2' '--domains' 'available' '--execute' \
                '--mboxes' 'urls' '--file-list' '/etc/hostname' '--sets' 'sets' '--domains' 'available' '--execute' \
                '--arch' 'arm64' \
                '--mboxes' '' '--sets' 'sets1' '--domains' 'available' '--execute' \
                '--mboxes' '' '--sets' 'sets2' '--domains' 'available' '--execute' \
                '--mboxes' 'urls' '--file-list' '/etc/hostname' '--sets' 'sets' '--domains' 'available'
        End

        It 'deals with both multiple domains and extra baseline run set patterns'
            patch_urls='urls'
            extra_baseline_run_set_patterns='sets1 sets2'
            domains='one two'
            When call kpet_generate_args '' 'sets' 's390x' 'arm64'
            The status should be success
            The array kpet_multiarch_args should be values \
                '--arch' 's390x' \
                '--mboxes' '' '--sets' 'sets1' '--domains' 'one' '--execute' \
                '--domains' 'two' '--execute' \
                '--mboxes' '' '--sets' 'sets2' '--domains' 'one' '--execute' \
                '--domains' 'two' '--execute' \
                '--mboxes' 'urls' '--file-list' '/etc/hostname' '--sets' 'sets' '--domains' 'one' '--execute' \
                '--domains' 'two' '--execute' \
                '--arch' 'arm64' \
                '--mboxes' '' '--sets' 'sets1' '--domains' 'one' '--execute' \
                '--domains' 'two' '--execute' \
                '--mboxes' '' '--sets' 'sets2' '--domains' 'one' '--execute' \
                '--domains' 'two' '--execute' \
                '--mboxes' 'urls' '--file-list' '/etc/hostname' '--sets' 'sets' '--domains' 'one' '--execute' \
                '--domains' 'two'
        End
    End
End
